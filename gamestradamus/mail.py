"""Basic email utilites
"""
from email.mime.text import MIMEText
import smtplib

from gamestradamus import cli

#-------------------------------------------------------------------------------
def connect_to_smtp(config=None):
    conf = cli.get_global_config(config)
    server = smtplib.SMTP(conf['gauge.smtp.host'], conf['gauge.smtp.port'])
    server.ehlo()
    if conf.get('gauge.smtp.use_tls', True):
        server.starttls()

    server.ehlo()
    server.login(conf['gauge.email.server_address'], conf['gauge.email.password'])
    return server

#-------------------------------------------------------------------------------
def sendmail(to_addrs, subject, body):
    server = connect_to_smtp()
    server_email = cli.get_global_config()['gauge.email.server_address']
    msg = MIMEText(body)

    msg['Subject'] = subject
    msg['From'] = server_email
    msg['To'] = ", ".join(to_addrs)
    server.sendmail(server_email, to_addrs, msg.as_string())
